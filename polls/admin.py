# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Choice, Question


class ChoiceInline(admin.TabularInline):
# class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 3



class QuestionAdmin(admin.ModelAdmin):
    # List display
    # https://docs.djangoproject.com/en/1.8/ref/contrib/admin/#django.contrib.admin.ModelAdmin.list_display
    list_display = ("question_text", "pub_date", "was_published_recently")

    # List filter
    # https://docs.djangoproject.com/en/1.8/ref/contrib/admin/#django.contrib.admin.ModelAdmin.list_filter
    list_filter = ["pub_date"]

    # Search fileds
    search_fields = ["question_text"]

    # Fields order.
    # fields = ["pub_date", "question_text"]
    # fields = ["question_text", "pub_date"]
    # Fields order with groups.
    fieldsets = [
        (None,          {"fields": ["question_text"]}),
        ("Date info",   {"fields": ["pub_date"]}),
    ]

    # Foreign keys
    inlines = [ChoiceInline]


# admin.site.register(Question)
admin.site.register(Question, QuestionAdmin)
