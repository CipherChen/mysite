# -*- coding: utf-8 -*-

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response

from .models import Question
from .models import Choice


def index(request):
    latest_question_list = Question.objects.order_by("-pub_date")[:5]

    return render_to_response("polls/index.html", {"latest_question_list": latest_question_list})


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)

    # Will generate csrf_token here.
    # So don't use render_to_response().
    return render(request, "polls/detail.html", {"question": question})


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render_to_response("polls/results.html", {"question": question})


def vote(request, question_id):
    p = get_object_or_404(Question, pk=question_id)

    try:
        selected_choice = p.choice_set.get(pk=request.POST["choice"])
    except (KeyError, Choice.DoesNotExist):
        return render_to_response("polls/detail.html", {"question": p, "error_message": "You didn't select a choice."})
    else:
        selected_choice.votes += 1
        selected_choice.save()

        return HttpResponseRedirect(reverse("polls:results", args=(p.id,)))
